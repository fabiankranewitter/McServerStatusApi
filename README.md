# McServerStatus

## ServerStatus

The `McServerStatus` class queries the status of an MC server. The client can present itself as different versions. The version numbers can be found [here](https://wiki.vg/Protocol_version_numbers). The return is formatted in JSON and the ping is also determined.
The `McJsonDecoder` decodes the individual data using Gson. `McColorConverter` converts the `getFormattedModt` into HTML formally e.g. B. to be displayed on a label in Android. Except for random chars, all format chars are supported. The timeout is specified in ms. [wiki.vg](https://wiki.vg/Protocol#Data_types) was a great help.

### Demo:
```
McServerStatus ss = new McServerStatus(new InetSocketAddress("S.MCCITYVILLE.DE", 25565), 5000, 47);
		
// fetch
ss.fetch();
		
// server status
System.out.println("getPing():" + ss.getPing());
System.out.println("getJson():"+ss.getJson());
		
McJsonDecoder cmc = new McJsonDecoder(ss);
		
// json
System.out.println("getJson():"+cmc.getJson());
		
// modt
System.out.println("getModt():"+cmc.getModt());
System.out.println("getFormatModt():"+cmc.getFormattedModt());
System.out.println(new McColorConverter().convertMotdtoHtml(cmc.getFormattedModt())); //convert modt to html
	    
// favicon
System.out.println("getFavicon():"+cmc.getFavicon());
	    
System.out.println("getOnlinePlayer():"+cmc.getOnlinePlayer());
System.out.println("getMaxPlayer():"+cmc.getMaxPlayer());
	    
System.out.println("getModType():"+cmc.getModType());
System.out.println("getProtocol():"+cmc.getProtocol());
System.out.println("getVersionName():"+cmc.getVersionName());
System.out.println("getModListe:");
cmc.getModListe().forEach(m->System.out.println(m+","));
```