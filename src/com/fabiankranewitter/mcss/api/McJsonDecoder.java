package com.fabiankranewitter.mcss.api;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class McJsonDecoder {

	// server
	private String json;
	
	private String formattedModt;
	private int maxPlayer;
	private int onlinePlayer;
	private String favicon;
	
	private String modt;
	private int protocol;

	private String versionName;
	private String modType;
	
	private List<String> modList = new ArrayList<String>();

	public McJsonDecoder(McServerStatus serverstatus) {
		Gson gson = new Gson();
		this.json = serverstatus.getJson();
		if (json == null) {
			throw new NullPointerException("json data from serverstatus is null");
		}
		JsonObject jsonObj = gson.fromJson(this.json, JsonObject.class);

		// version
		if (null != jsonObj.get("version")) {
			JsonObject version = jsonObj.get("version").getAsJsonObject();
			this.protocol = version.get("protocol").getAsInt();
			this.versionName = version.get("name").getAsString();
		}

		// players
		if (null != jsonObj.get("players")) {
			JsonObject players = jsonObj.get("players").getAsJsonObject();
			this.maxPlayer = players.get("max").getAsInt();
			this.onlinePlayer = players.get("online").getAsInt();
		}

		// mods
		if (null != jsonObj.get("modinfo")) {
			JsonObject modInfo = jsonObj.get("modinfo").getAsJsonObject();
			this.modType = modInfo.get("type").getAsString();
			modInfo.get("modList").getAsJsonArray().forEach(e -> this.modList.add(e.getAsString()));
		}

		// favicon
		JsonElement fa = jsonObj.get("favicon");
		if (fa != null) {
			favicon = fa.getAsString();
		}

		// description
		if (jsonObj.get("description") != null) {
			this.modt = jsonObj.get("description").toString();
			if (!jsonObj.get("description").isJsonObject()) {
				this.formattedModt = jsonObj.get("description").getAsString();
			} else {
				JsonObject descriptionJObj = jsonObj.get("description").getAsJsonObject();
				if (descriptionJObj.get("extra") != null && descriptionJObj.get("extra").isJsonArray()) {
					JsonArray array = descriptionJObj.get("extra").getAsJsonArray();
					this.formattedModt = "";
					for (int i = 0; array.size() > i; i++) {
						if (!array.get(i).isJsonObject()) {
							this.formattedModt += "§r" + array.get(i).getAsString();
							continue;
						}
						JsonObject ao = array.get(i).getAsJsonObject();
						if (ao.get("bold") != null && ao.get("bold").getAsBoolean()) {
							this.formattedModt += "§r§l" + convertNametoMcColor(ao.get("color").getAsString())
									+ ao.get("text").getAsString();
						} else if (ao.get("italic") != null && ao.get("italic").getAsBoolean()) {
							this.formattedModt += "§r§o" + convertNametoMcColor(ao.get("color").getAsString())
									+ ao.get("text").getAsString();
						} else if (ao.get("color") != null) {
							this.formattedModt += "§r" + convertNametoMcColor(ao.get("color").getAsString())
									+ ao.get("text").getAsString();
						} else {
							formattedModt += "§r" + ao.get("text").getAsString();
						}
					}
				} else if (descriptionJObj.get("text") != null) {
					formattedModt = descriptionJObj.get("text").getAsString();
				}
			}
		}
	}

	String getJson() {
		return json;
	}

	public String getFavicon() {
		return favicon;
	}

	public String getFormattedModt() {
		return formattedModt;
	}

	public String getModt() {
		return modt;
	}

	public int getMaxPlayer() {
		return maxPlayer;
	}

	public int getProtocol() {
		return protocol;
	}

	public int getOnlinePlayer() {
		return onlinePlayer;
	}

	public String getVersionName() {
		return versionName;
	}

	public List<String> getModListe() {
		return modList;
	}

	public String getModType() {
		return modType;
	}
	
	
	private String convertNametoMcColor(String color) {
		switch (color.toLowerCase()) {
			case "black": return "§0";
			case "dark_blue": return "§1";
			case "dark_green": return "§2";
			case "dark_aqua": return "§3";
			case "dark_red": return "§4";
			case "dark_purple": return "§5";
			case "gold": return "§6";
			case "gray": return "§7";
			case "dark_gray": return "§8";
			case "blue": return "§9";
			case "green": return "§a";
			case "aqua": return "§b";
			case "red": return "§c";
			case "light_purple":return "§d";
			case "yellow": return "§e";
			case "white": return "§f";
		default:
			return null;
		}
	}
}
