package com.fabiankranewitter.mcss.api;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/*
 * https://wiki.vg/Server_List_Ping
 */
public class McServerStatus {

	private InetSocketAddress host;
	private int timeout = 5000;
	private int version;
	private long ping;
	private String json;

	McServerStatus(InetSocketAddress host,int version) {
		this.host = host;
		this.version = version;
	}
	
	McServerStatus(InetSocketAddress host, int timeout,int version) {
		this(host, version);
		this.timeout = timeout;
	}

	 public void fetch() throws IOException {
		 	Socket socket = new Socket();
			socket.connect(this.host, timeout);
			DataOutputStream dataOutStream = new DataOutputStream(socket.getOutputStream());
			DataInputStream dataInStream = new DataInputStream(socket.getInputStream());
			
			// handshake (client to server)
			McPacketBuffer handshakePacket = new McPacketBuffer(0x00);
			handshakePacket.writeVarInt(this.version); // write protocol version (1.7=4,1.8 = 47)
			handshakePacket.writeString(this.host.getHostString()); // write host
			handshakePacket.writeShort(this.host.getPort()); // write port
			handshakePacket.writeVarInt(1); //set status state (1 for status, 2 for login)		
			handshakePacket.writeTo(dataOutStream);
			handshakePacket.close();
			
			// request (client to server)
			McPacketBuffer requestPacket = new McPacketBuffer(0x00);
			requestPacket.writeTo(dataOutStream);
			requestPacket.close();
			
			// response (server to client)
			McPacketBuffer.readVarInt(dataInStream); //size
			int packetId = McPacketBuffer.readVarInt(dataInStream);

			if (packetId == -1) {
				socket.close();
		        throw new IOException("end of stream");
		    }
			
			if (packetId != 0x00) {
				socket.close();
		        throw new IOException("invalid packet id");
		    }
			
			int jsonLength = McPacketBuffer.readVarInt(dataInStream);

			if (jsonLength == -1) {
				socket.close();
		        throw new IOException("end of stream.");
		    }

		    if (jsonLength == 0) {
		    	socket.close();
		        throw new IOException("invalid string length");
		    }

			this.json = McPacketBuffer.readString(dataInStream, jsonLength);

			// ping (client to server)
			long now = System.currentTimeMillis();
			
			McPacketBuffer pingPacket = new McPacketBuffer(0x01);
			pingPacket.writeLong(now);
			pingPacket.writeTo(dataOutStream);
			pingPacket.close();

			// pong (server to client)
			McPacketBuffer.readVarInt(dataInStream); //size
			packetId = McPacketBuffer.readVarInt(dataInStream);
			if (packetId == -1) {
				socket.close();
				throw new IOException("end of stream");
			}

			if (0x01 != packetId) {
				socket.close();
				throw new IOException("invalid packet id");
			}
			//calculate ping
			this.ping = now - dataInStream.readLong();
			socket.close();
	}
	
	public InetSocketAddress getHost() {
		return host;
	}

	public int getTimeout() {
		return timeout;
	}

	public String getJson(){
		return json;
	}
	
	public long getPing() {
		return ping;
	}	
}
